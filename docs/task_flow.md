```plantuml
@startuml
User_Browser -> Api_server: Request to create a task
Api_server -> Database: create new message task
Api_server -> MQ: TASK_RECEIVED
User_Browser --> Api_server: [Optional] Upload media file
Api_server -> Media_Server: Upload file
Media_Server -> Api_server: URL of uploaded media file
Api_server -> User_Browser: URL of uploaded media file
Api_server -> User_Browser: new message task succ



Scheduler --> Scheduler: [recurring] find tasks need to be processed
Scheduler -> MQ: TASK_CREATED
MQ --> TaskManager: TASK_CREATED
TaskManager -> MQ: JOB_CREATED(sms), could be extended in the future
MQ --> Sms_Notifier: JOB_CREATED_SMS
Sms_Notifier-> Twillo: send message
Twillo -> User_Phone: send message


Twillo --> Receiver:  message status updated
Media_Server --> Twillo: [optional] retrive media files
Receiver -> MQ: SMS_STATUS_CHANGED
MQ --> Sms_Notifier: SMS_STATUS_CHANGED
Sms_Notifier -> MQ : JOB_COMPLETED
MQ --> Api_server: JOB_COMPLETED
Api_server --> User_Browser: [WebSocket] SMS status updated
@enduml
```